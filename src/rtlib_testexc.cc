/**
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rtlib_testexc.h"

#include <cstdio>
#include <bbque/utils/utility.h>

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "aem.app"
#undef  BBQUE_LOG_UID
#define BBQUE_LOG_UID GetChUid()

TestWorkload::TestWorkload(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib,
		uint32_t cycle_ms,
		uint32_t cycles_total) :
	BbqueEXC(name, recipe, rtlib),
	cycle_ms(cycle_ms),
	cycles_total(cycles_total) {

	// NOTE: since RTLib 1.1 the derived class construct should be used
	// mainly to specify instantiation parameters. All resource
	// acquisition, especially threads creation, should be palced into the
	// new onSetup() method.

	logger->Notice("New TestWorkload with: cycle time %d[ms], cycles count %d",
			cycle_ms, cycles_total);

	logger->Info("EXC Unique IDentifier (UID): %u", GetUniqueID());
}

RTLIB_ExitCode_t TestWorkload::onSetup() {
	// This is just an empty method in the current implementation of this
	// testing application. However, this is intended to collect all the
	// application specific initialization code, especially the code which
	// acquire system resources (e.g. thread creation)
	logger->Notice("TestWorkload::onSetup():");
	return RTLIB_OK;
}

RTLIB_ExitCode_t TestWorkload::onConfigure(int8_t awm_id) {

	logger->Notice("TestWorkload::onConfigure(): "
		"EXC [%s], AWM[%02d]", exc_name.c_str(), awm_id);

	// Explicit query on the amount of assigned processing cores and memory
	int nr_cpu_cores, cpu_quota, mem;
	int nr_cpus, nr_gpus;
	GetAssignedResources(CPU, nr_cpus);
	GetAssignedResources(GPU, nr_gpus);
	GetAssignedResources(PROC_NR, nr_cpu_cores);
	GetAssignedResources(PROC_ELEMENT, cpu_quota);
	GetAssignedResources(MEMORY, mem);
	logger->Notice("TestWorkload::onConfigure(): "
		"EXC [%s], AWM[%02d] => <cpu_quota>=%3d, <nr_cpu_cores>=%2d, <memory>=%3d"
		" <nr_cpus>=%2d, <nr_gpus>=%2d",
		exc_name.c_str(), awm_id, cpu_quota, nr_cpu_cores, mem, nr_cpus, nr_gpus);

	// Dealing with unmanaged mode
	if (nr_cpu_cores < 0)
		nr_cpu_cores = 1;

	// Simulate the execution cycle duration, according to the assigned AWM
	cycle_length = 1000 * cycle_ms * (n_core_max / nr_cpu_cores);
	logger->Notice("TestWorkload::onConfigure(): "
		"EXC [%s], AWM[%02d] => cycle time %d[ms]",
		exc_name.c_str(), awm_id, cycle_length/1000);
	SetMinimumCycleTimeUs(cycle_length);
	return RTLIB_OK;
}

RTLIB_ExitCode_t TestWorkload::onRun() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	// Return as soon as we completed the required cycles
	if (Cycles() >= cycles_total)
		return RTLIB_EXC_WORKLOAD_NONE;

	// Do one more cycle
	logger->Notice("TestWorkload::onRun(): "
		"EXC [%s], AWM[%02d] => processing %d[ms]",
		exc_name.c_str(), wmp.awm_id, cycle_length/1000);

	// NOTE: The CPS configuration will ensure a proper cycle-rate even if
	// this method is really "short"

	return RTLIB_OK;
}

RTLIB_ExitCode_t TestWorkload::onMonitor() {
	RTLIB_WorkingModeParams_t const wmp = WorkingModeParams();

	logger->Notice("TestWorkload::onMonitor(): "
		"EXC [%s], AWM[%02d] => cycles [%d/%d], CPS = %.2f",
		exc_name.c_str(), wmp.awm_id, Cycles(), cycles_total, GetCPS());

	return RTLIB_OK;
}

