/**
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "rtlib_testexc.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>

#define EXC_BASENAME "exc"
#define RCP_BASENAME "exRecipe"

// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "app"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;

/**
 * @brief An entry of the map collecting managed EXCs
 */
typedef std::pair<std::string, pBbqueEXC_t> excMapEntry_t;

/**
 * @brief Maps recipes on corresponding EXCs
 */
typedef std::map<std::string, pBbqueEXC_t> excMap_t;

/**
 * The decription of each testapp parameters
 */
po::options_description opts_desc("TestApp Configuration Options");

/**
 * The map of all testapp parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The map of managed EXCs
 */
excMap_t exc_map;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/rtlib_testapp.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @breif The number of EXCs to spaws
 */
unsigned short exc_count;

/**
 * @brief The total workload processing time considering the Best Case
 * Execution (BCE) time
 */
unsigned int workload_time;

/**
 * @brief The number of processing cycles required for the complete workload
 * processing
 */
unsigned int cycles_count;

void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "Barbeque RTLib TestApp (ver. " <<
			g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

/**
 * @brief Register the required EXCs and enable them
 *
 * @param name the recipe to use
 */
RTLIB_ExitCode_t SetupEXCs(uint8_t exc_count, const char *recipe) {
	char exc_name[] = EXC_BASENAME "_00";
	unsigned int cycle_time;
	excMap_t::iterator it;
	pBbqueEXC_t pexc;

	logger->Info("STEP 1. Registering [%03d] EXCs, using recipe [%s]...",
			exc_count, recipe);

	for (uint8_t i = 0; i < exc_count; ++i) {

		// Setup EXC name and recipe name
		::snprintf(exc_name, ::strlen(exc_name)+1, EXC_BASENAME "_%02d", i);

		// Build a new EXC (without enabling it yet)
		assert(rtlib);

		cycle_time = workload_time*1000/cycles_count;
		pexc = pBbqueEXC_t(new TestWorkload(exc_name, recipe, rtlib,
					cycle_time, cycles_count));

		// Saving the EXC (if registration to BBQ was successfull)
		if (pexc->isRegistered())
			exc_map.insert(excMapEntry_t(exc_name, pexc));

	}

	logger->Info("STEP 3. Starting [%03u] EXCs control threads...",
			(unsigned)exc_map.size());

	it = exc_map.begin();
	for ( ; it != exc_map.end(); ++it) {
		pexc = (*it).second;

		// Starting the control thread for the specified EXC
		assert(pexc);
		pexc->Start();
	}

	return RTLIB_OK;
}

/**
 * @brief Wait the completion of all the EXCs
 */
RTLIB_ExitCode_t RunEXCs() {
	excMap_t::iterator it;
	pBbqueEXC_t pexc;

	logger->Info("STEP 4. Running [%03u] control threads...",
			(unsigned)exc_map.size());

	it = exc_map.begin();
	for ( ; it != exc_map.end(); ++it) {
		pexc = (*it).second;
		pexc->WaitCompletion();
	}

	return RTLIB_OK;
}

/**
 * @brief Unregister all the EXCs
 */
RTLIB_ExitCode_t DestroyEXCs() {
	logger->Info("STEP 5. Disabling [%03u] EXCs...",
			(unsigned)exc_map.size());
	exc_map.clear();
	return RTLIB_OK;
}

int main(int argc, char *argv[]) {

	opts_desc.add_options()
		("help,h", "print this help message")
		("version,v", "print program version")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"application configuration file")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("BbqRTLibTestApp"),
			"recipe name (for all EXCs)")
		("exc,e", po::value<unsigned short>(&exc_count)->
			default_value(1),
			"number of EXC to spawm")

		("workload,w", po::value<unsigned int>(&workload_time)->
			default_value(1),
			"overall BCE time [s]")
		("cycles,c", po::value<unsigned int>(&cycles_count)->
			default_value(5),
			"workload cycles count")

	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("rtlib_testapp");

	// Parse command line options
	ParseCommandLine(argc, argv);

	// Welcome screen
	logger->Info("Barbeque RTLib TestApp (ver. %s) ::", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__ );

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib library, application [%s]...",
			::basename(argv[0]));

	RTLIB_Init(::basename(argv[0]), &rtlib);
	assert(rtlib);

	// Configuring required Execution Contexts
	SetupEXCs(exc_count, recipe.c_str());

	// Wait for wrokload processing to terminate
	RunEXCs();

	// Releasing all the EXCs
	DestroyEXCs();

	logger->Info("===== RTLibTestApp DONE! =====");
	return EXIT_SUCCESS;

}

